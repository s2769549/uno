package UNO.Client.Application;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class Client {
    public String serverAddress = "localhost";


    public static void main(String[] args) throws InterruptedException {

        System.out.println("\n" +
                "██╗   ██╗  ███╗   ██╗   ██████╗ \n" +
                "██║   ██║  ████╗  ██║  ██╔═══██╗\n" +
                "██║   ██║  ██╔██╗ ██║  ██║   ██║\n" +
                "██║   ██║  ██║╚██╗██║  ██║   ██║\n" +
                "╚██████╔╝  ██║ ╚████║  ╚██████╔╝\n" +
                " ╚═════╝   ╚═╝  ╚═══╝   ╚═════╝ - The Game\n" +
                "                            \n");
        TimeUnit.MILLISECONDS.sleep(1000);
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        TimeUnit.MILLISECONDS.sleep(700);
        System.out.println("CLIENT | -> preparing");
        TimeUnit.MILLISECONDS.sleep(700);
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");


        TimeUnit.MILLISECONDS.sleep(2000);
        System.out.println(" ");
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        TimeUnit.MILLISECONDS.sleep(700);
        System.out.println("CLIENT | -> successfully prepared");
        TimeUnit.MILLISECONDS.sleep(700);
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        TimeUnit.MILLISECONDS.sleep(2000);
        System.out.println(" ");
        Scanner portScanner = new Scanner(System.in);

        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        TimeUnit.MILLISECONDS.sleep(700);
        System.out.println("CLIENT | -> Please provide the desired port");
        TimeUnit.MILLISECONDS.sleep(700);
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        int port = portScanner.nextInt();

        TimeUnit.MILLISECONDS.sleep(2000);
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        TimeUnit.MILLISECONDS.sleep(700);
        System.out.println("CLIENT | -> Establishing connection...");
        TimeUnit.MILLISECONDS.sleep(2000);
        System.out.println("CLIENT | -> Connection established successfully");
        TimeUnit.MILLISECONDS.sleep(700);
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

        TimeUnit.MILLISECONDS.sleep(2000);

        String username = "empty";
        Scanner scanner = new Scanner(System.in);
        System.out.println(" ");
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        TimeUnit.MILLISECONDS.sleep(700);
        System.out.println("CLIENT | -> Please input the server address");
        TimeUnit.MILLISECONDS.sleep(700);
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

        String serverAddress = scanner.nextLine();


        TimeUnit.MILLISECONDS.sleep(2000);
        System.out.println(" ");
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        TimeUnit.MILLISECONDS.sleep(700);
        System.out.println("CLIENT | -> Connected to the server");
        TimeUnit.MILLISECONDS.sleep(700);
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

        TimeUnit.MILLISECONDS.sleep(2000);


        try (Socket connection = new Socket(serverAddress, port)) {

            PrintWriter out = new PrintWriter(connection.getOutputStream(), true);
            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String inputString;
            String writeString;
            ClientHandler client = new ClientHandler(connection);
            new Thread(client).start();

            String card = "";
            do {
                if (username.equals("empty")) {
                    System.out.println(" ");
                    System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
                    TimeUnit.MILLISECONDS.sleep(700);
                    System.out.println("SERVER | -> Please enter a username");
                    TimeUnit.MILLISECONDS.sleep(700);
                    System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

                    inputString = scanner.nextLine();
                    username = inputString;
                    out.println("SERVER | -> Username :" + inputString);
                    System.out.println(" ");
                    System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
                    TimeUnit.MILLISECONDS.sleep(700);
                    System.out.println("SERVER | -> Your username is: " + username);
                    TimeUnit.MILLISECONDS.sleep(700);
                    System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

                    client.doMakeHandshake(username,"human_player");

                } else {
                    inputString = scanner.nextLine();
                    String [] inputSplit = inputString.split("/");
                    System.out.println(" ");
                    System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
                    System.out.println("SERVER | input | ->" + inputString);
                    System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

                    switch (inputSplit[0].toLowerCase().trim()) {
                        case "start":
                            client.doStartGame(inputSplit[1]);
                            break;
                        case "add":
                            String [] inputSplitSplit = inputSplit[1].trim().split(" ");
                            client.doAddComputerPlayer(inputSplitSplit[0], inputSplitSplit[1]);
                            break;
                        case "play":
                            card = inputSplit[1];
                            client.doPlayCard(inputSplit[1]);
                            break;
                        case "draw":
                            client.doDrawCard();
                            break;
                        case "leave":
                            client.doLeaveGame();
                            break;
                        case "create":
                            client.doCreateLobby(inputSplit[3]);
                            break;
                        case "join":
                            client.doJoinLobby(inputSplit[1]);
                            break;
                        case "send":
                            client.doSendMessage(inputString.replace("send",""));
                            break;
                        case "wish":
                            client.doPlayCard(card.toLowerCase().replace("black", inputSplit[1]).toUpperCase());
                            break;
                            case "remove":
                                client.doRemovePlayer(inputSplit[1]);
                                break;
                        default:
                            System.out.println(" ");
                            System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
                            TimeUnit.MILLISECONDS.sleep(700);
                            System.out.println("SERVER | -> To start the Game type: start / progressive. To add a Computer player type: add / [name] computer_player. To play a Card type: play --> for example: blue 7 / To draw a card type: draw / To leave the Game type: leave / To create a Game type: create lobbyName / To join a Lobby type: join lobbyName / To send a message type: send message");
                            TimeUnit.MILLISECONDS.sleep(700);
                            System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

                    }
                }
            } while (!inputString.equalsIgnoreCase("exit"));

        } catch (Exception e) {
            System.out.println(" ");
            System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
            TimeUnit.MILLISECONDS.sleep(700);
            System.out.println("SERVER | -> error " + e);
            TimeUnit.MILLISECONDS.sleep(700);
            System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

        }
    }
}