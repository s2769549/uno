package UNO.Client.Application;

import UNO.Server.Controller.ServerProtocol;

import java.io.*;
import java.net.Socket;
import java.util.concurrent.TimeUnit;

public class ClientHandler implements Runnable,ClientProtocol {

    private Socket socket;
    private BufferedReader incoming;
    private PrintWriter sender;
    public ClientHandler(Socket connection) throws IOException {
        this.socket = connection;
        this.incoming = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        this.sender = new PrintWriter(connection.getOutputStream(),true);
    }



    @Override
    public void run() {
        try {
            while (true) {
                String message = incoming.readLine();
               if (message != null) {
                    String[] splitter = message.split("\\|");

                    switch (splitter[0]) {
                        case "AH":
                            handleAcceptHandshake();
                            break;
                        case "IAD":
                            handleInformAdmin();
                            break;
                        case "BPJ":
                            handleBroadcastPlayerJoined(splitter[1]);
                            break;
                        case "GST":
                            handleBroadcastGameStarted(splitter[1]);
                            break;
                        case "RST":
                            handleBroadcastRoundStarted();
                            break;
                        case "BGI":
                            handleBroadcastGameInformation(splitter[1], splitter[2], splitter[3], splitter[4]);
                            break;
                        case "BCP":
                            handleBroadcastCardPlayed(splitter[1], splitter[2]);
                            break;
                        case "CW":
                            handleColorWish(splitter[1]);
                            break;
                        case "BDC":
                            handleBroadcastDrewCard(splitter[1]);
                            break;
                        case "BTS":
                            handleBroadcastTurnSkipped(splitter[1]);
                            break;
                        case "BRS":
                            handleBroadcastReverse(splitter[1]);
                            break;
                        case "BLG":
                            handleBroadcastLeftGame(splitter[1]);
                            break;
                        case "RP":
                            handleRemindPlay(splitter[1]);
                            break;
                        case "RE":
                            handleRoundEnded(splitter[1]);
                            break;
                        case "GE":
                            handleGameEnded(splitter[1]);
                            break;
                        case "ERR":
                            handleSendErrorCode(splitter[1]);
                            break;
                        case "DPC":
                            handleDrewPlayableCard(splitter[1]);
                            break;
                        case "AC":
                            handleAskColor();
                            break;
                        case "BCC":
                            handleBroadcastColorChange(splitter[1]);
                            break;
                        case "BGM":
                            handleBroadcastGameMessage();
                            break;
                        default:
                            sender.println("Command is not recognized by CLIENT");
                            break;
                    }
                }
            }
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void handleAcceptHandshake() {
        System.out.println(" ");
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        System.out.println("UNO | -> Handshake successfully established. \n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        System.out.println(" ");

    }

    @Override
    public void handleInformAdmin() {
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        System.out.println("UNO | -> You are the admin. To start a Game type: start / progressive.\nUNO | ->  To add a Computer player type: add / [name] computer_player.");
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
    }

    public void handleColorWish(String color){
        System.out.println("The color wish is: " + color);
    }

    @Override
    public void handleBroadcastPlayerJoined(String playerName) {
        System.out.println(" ");
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        System.out.println("UNO | -> " + playerName + " has joined.\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        System.out.println(" ");

    }

    @Override
    public void handleBroadcastGameStarted(String gameMode) {
        System.out.println(" ");
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        System.out.println("UNO | -> Current game mode is: " + gameMode + "\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\"");
        System.out.println(" ");
    }

    @Override
    public void handleBroadcastRoundStarted() throws InterruptedException {

    }
    @Override
    public void handleBroadcastGameInformation(String topCard, String playerHand, String playersList, String isYourTurn) {
        System.out.println(" ");
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        System.out.println("UNO | HELP -> To play a normal card : play / [color] [number] - example: play / blue 6");
        System.out.println("UNO | HELP -> To play a special card: play / [color] [the name in letters of the wanted card] - example: play / blue reverse or play / black wild");
        System.out.println("UNO | HELP -> In the case the desired card is [BLACK / WILDPLUS4]: play / [wanted color] draw_4");
        System.out.println("UNO | HELP -> To draw a card: draw");
        System.out.println(" ");
        System.out.println("UNO | -> Top Card: " + topCard);
        System.out.println("UNO | -> Player Hand: " + playerHand);
        System.out.println("UNO | -> Player List: " + playersList);
        System.out.println("UNO | -> Turn: " + isYourTurn);
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

        String[] playersListSplit = playersList.split(";");
        for (int n = 0; n<playersListSplit.length - 1;n++){
            String[] playerSplit = playersListSplit[n].split(":");
            int total = 20 - playerSplit[0].length();
            String result = playerSplit[0];
            for (int y = 0; y<total;y++){
                result = result + " ";
            }
            result = result + playerSplit[1] + "     " + playerSplit[2];
            System.out.println("UNO | -> " + result);
        }

    }

    @Override
    public void handleBroadcastCardPlayed(String playerName, String playedCard) {
        System.out.println(" ");
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        System.out.println("UNO | -> " + playerName + " has played " + playedCard);
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

    }

    @Override
    public void handleBroadcastDrewCard(String playerName) {
        System.out.println(" ");
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        System.out.println("UNO | -> " + playerName + " has drawn a card.");
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

    }

    @Override
    public void handleBroadcastTurnSkipped(String playerName) {
        System.out.println(" ");
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        System.out.println("UNO | -> " + playerName + " has been skipped!");
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

    }

    @Override
    public void handleBroadcastReverse(String direction) {
        System.out.println(" ");
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        System.out.println("UNO | -> The direction has changed.");
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

    }

    @Override
    public void handleBroadcastLeftGame(String playerName) {
        System.out.println(" ");
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        System.out.println("UNO | -> " + playerName + " has left the game");
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

    }

    @Override
    public void handleRemindPlay(String timeLeft) {
        System.out.println(" ");
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        System.out.println("UNO | -> " + timeLeft + " left");
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

    }

    @Override
    public void handleRoundEnded(String playerName) {
        System.out.println(" ");
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        System.out.println("UNO | -> Round ended!");
        System.out.println("UNO | ROUND WINNER -> " + playerName);
        System.out.println("UNO | -> To Start the next Round type Start/[gamemode]");
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

    }

    @Override
    public void handleGameEnded(String playerName) {
        System.out.println(" ");
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        System.out.println("UNO | -> Game has ended!");
        System.out.println("UNO | GAME WINNER -> " + playerName);
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

    }

    @Override
    public void handleSendErrorCode(String errorCode) {
        System.out.println(" ");
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        System.out.println("UNO:ERROR | -> " + ServerProtocol.Errors.valueOf(errorCode).getMessage());
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

    }

    @Override
    public void handleBroadcastListOfLobbies(String lobbiesList) {
        System.out.println("UNO | -> Lobby list: " + lobbiesList);
    }

    @Override
    public void handleBroadcastCreatedLobby(String lobbyName) {
        System.out.println("UNO | -> Lobby: " + lobbyName + "has been created.");
    }

    @Override
    public void handleBroadcastPlayerJoinedLobby(String playerName) {
        System.out.println("UNO | -> " + playerName + " has joined the Lobby");
    }

    @Override
    public void handleBroadcastMessage(String message) throws InterruptedException {
        System.out.println(" ");
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        System.out.println("UNO | -> Get ready, game is starting in...");
        TimeUnit.MILLISECONDS.sleep(1500);
        System.out.println("UNO | -> 3");
        TimeUnit.MILLISECONDS.sleep(1500);
        System.out.println("UNO | -> 2");
        TimeUnit.MILLISECONDS.sleep(1500);
        System.out.println("UNO | -> 1");
        TimeUnit.MILLISECONDS.sleep(1500);
        System.out.println("UNO | -> Let the game begin!");
        TimeUnit.MILLISECONDS.sleep(1500);
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

    }


    @Override
    public void handleBroadcastSayUNO() {
        System.out.println("Uno");
    }

    @Override
    public void handleDrewPlayableCard(String playableCard) {
        System.out.println(" ");
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        System.out.println("Played card -> " + playableCard);
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

    }

    @Override
    public void handleAskColor() {
        System.out.println(" ");
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        System.out.println("Please chose a color...");
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

    }

    @Override
    public void handleBroadcastColorChange(String color) {
        System.out.println(" ");
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        System.out.println("Color has been changed to -> " + color);
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

    }

    @Override
    public void handleBroadcastGameMessage(String... args) throws InterruptedException {
        System.out.println(" ");
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        System.out.println("UNO | -> Round will be starting soon...");
        TimeUnit.MILLISECONDS.sleep(2000);
        System.out.println("UNO | -> Get Ready!");
        TimeUnit.MILLISECONDS.sleep(1000);
        System.out.println("UNO | -> 3");
        TimeUnit.MILLISECONDS.sleep(1000);
        System.out.println("UNO | -> 2");
        TimeUnit.MILLISECONDS.sleep(1000);
        System.out.println("UNO | -> 1");
        TimeUnit.MILLISECONDS.sleep(1000);
        System.out.println("UNO | -> GO!");
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

    }
    public void doRemovePlayer(String playerName) {
        sender.println("RP|" + playerName);

    }

    @Override
    public void doMakeHandshake(String playerName, String playerType) {
        sender.println(ClientCommand.MH + "|" + playerName + "|" + playerType);
    }

    @Override
    public void doAddComputerPlayer(String playerName, String strategy) {
        sender.println(ClientCommand.ACP + "|" + playerName + "|" + strategy);
        sender.flush();
    }

    @Override
    public void doStartGame(String gameMode) {
        sender.println(ClientCommand.SG +"|" + gameMode);
        sender.flush();
    }

    @Override
    public void doPlayCard(String card) {
        sender.println(ClientCommand.PC + "|" + card);
        sender.flush();
    }

    @Override
    public void doDrawCard() {
        sender.println(ClientCommand.DC);
        sender.flush();
    }

    @Override
    public void doLeaveGame() {
        sender.println(ClientCommand.LG);
        sender.flush();
    }

    @Override
    public void doCreateLobby(String lobbyName) {
        sender.println(ClientCommand.CL + "|" + lobbyName);
        sender.flush();
    }

    @Override
    public void doJoinLobby(String lobbyName) {
        sender.println(ClientCommand.JL + "|" + lobbyName);
        sender.flush();
    }

    @Override
    public void doSendMessage(String message) {
        sender.println(ClientCommand.SM + "|" + message);
        sender.flush();
    }

    @Override
    public void doSayUno() {
    }

    @Override
    public void doRetainCard(String choice) {
        sender.println(ClientCommand.RC + "|" + choice);
        sender.flush();
    }

    @Override
    public void doColorChoice(String color) {
        System.out.println("Color choice -> " + color);
        sender.println(ClientCommand.CC + "|" + color);
        sender.flush();
    }

    @Override
    public void doMakeChoiceSeven(String playerName, String card) {
        sender.println(ClientCommand.MC7 + "|" + playerName + "|" + card);
        sender.flush();
    }
}
