package UNO.Client.View;

import UNO.Client.Model.Player.ComputerPlayer;
import UNO.Client.Model.Factory.Player;
import UNO.Client.Model.Player.HumanPlayer;
import UNO.Server.Controller.Game;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;


public class TUI {
    private static ArrayList<String> usernames = new ArrayList<>();

    static String[] listOfUsernames = {"Olivia" ,"Emma","Liam","Olivia", "Noah", "Emma","Oliver","Charlotte","Elijah","Amelia","James","Ava","William","Sophia","Benjamin","Isabella","Lucas","Mia","Henry","Evelyn","Theodore","Harper"};

    /**
     * removes the spaces from the usernames
     * @param splitUsername the usernames that are split
     * @return an array of the usernames without spaces
     */
    private static String[] removeSpaces(String[] splitUsername) {
        for (int i = 0; i < splitUsername.length; i++) {
            splitUsername[i] = splitUsername[i].trim();
        }
        return splitUsername;
    }

    /**
     * generates a random username for the ComputerPlayer
     * one of the usernames of the List of usernames
     * @return a random username
     */
    public static String randomUsername(Game game) {
        boolean usernameExists = true;
        String usernameComp = null;
        int random = 0;
        while (usernameExists) {
            random = (int) (Math.random() * usernames.size());
            usernameComp = usernames.get(random);
            for (Player player : game.getPlayers()) {
                if (player.getUsername().equals(usernameComp)) {
                    break;
                } else {
                    usernameExists = false;
                }
            }
        }
        listOfUsernames[random] = null;
        usernameComp = usernameComp + "-Computer";
        return usernameComp;
    }

    /**
     * Scans the Input of the User.
     * @return the input of the user.
     */
    public static String scannerInput(){
        Scanner scanner = new Scanner(System.in);
        return scanner.nextLine().trim();
    }

    /**
     * aks the Admin if the Players that are inserted are correct
     * if correcter than goes onto play
     * if not correct the admin is able to remove or add players
     */
    public static void removeAddPlayers(Game game) {
        boolean correctPlayers = false;
        int message = 0;
        while (!correctPlayers) {
                if (game.getPlayers().size() > 1){
                    System.out.println("Players that are currently in the game: " + game.getPlayers());
                    System.out.println("Are these players correct? Are these all Players? (y/n)");
                    correctPlayers = Game.inputIntoBoolean(scannerInput().toLowerCase());
                }
                if (!correctPlayers) {
                    if (game.getPlayers().size() == 1){
                        System.out.println("add another Player");
                    }else {
                        System.out.println("If you want to add a player write: add username\nIf you want to remove a player use: remove username\nTo add a Computer write: -C and to remove write the full name of the Computer");
                    }
                boolean correctInput = false;
                String[] splitInput = new String[0];
                while (!correctInput){
                    splitInput = scannerInput().split(" ");
                    removeSpaces(splitInput);
                    if (splitInput.length == 2){
                        correctInput = true;
                    }
                }
                String player = splitInput[1];
                boolean wrongInput = false;
                while (!wrongInput) {
                    switch (splitInput[0].toLowerCase()) {
                        case "add":
                            if (Objects.equals(player.toLowerCase(), "-c")) {
                                String usernameComputer = randomUsername(game);
                                game.addPlayer(new ComputerPlayer(usernameComputer));
                            } else {
                                game.addPlayer(new HumanPlayer(player));

                            }
                            wrongInput = true;
                            break;
                        case "remove":
                            game.removePlayer(player);
                            wrongInput = true;
                            break;
                        default:
                            System.out.println("Wrong Input");

                            break;
                    }
                }
            }
        }
    }

        /**
         * this method will add or remove players from the game
         * @param game the game that is currently being played
         */
    public static void setUpPlayers(Game game){
        boolean checker = true;
        if (game.getPlayers().size() > 0){
            System.out.println(game.getPlayers() + " are already in the game");
            System.out.println("Do you really want to remove all Players otherwise use the add/remove command? (y/n)");
            boolean removePlayers = Game.inputIntoBoolean(scannerInput().toLowerCase());
            if (removePlayers){
                game.removeAllPlayers();
            }else {
                checker = false;
            }
        }
        while (checker) {
            System.out.println("Enter Usernames: like Alex, Sebastian");
            System.out.println("If you want to add a Computer Player, write: -C");
            String username = scannerInput();
            String[] splitUsername = username.split(",");
            removeSpaces(splitUsername);
            if (splitUsername.length < 10) {
                System.out.println(2);
                for (String s : splitUsername) {
                    System.out.println(3);
                    if (Objects.equals(s.toLowerCase(), "-c")){
                        String usernameComputer = randomUsername(game);
                        game.addPlayer(new ComputerPlayer(usernameComputer));
                    }else {
                        game.addPlayer(new HumanPlayer(s));
                    }
                }
                checker = false;
            } else {
                System.out.println("You can Play with maximum 10 Players.");
            }
        }
        removeAddPlayers(game);
    }
    /**
     * To Start the game with all options
     * @param args the arguments
     */
    public static void main(String[] args){
        usernames.addAll(List.of(listOfUsernames));
        String options = "Chose from the following options: \nstart - starts the game\nsetUp - to put in all names\nadd/remove - to add or remove Players in the Game\nsettings - to change the amount of Card that will be handed out\nrules - to show a short summary\ninfo - to see the current Players\nexit- close the game";
        String rules = "The Rules of Uno are:\nYou can only put the same colour or value on top of each other\nThe only exceptions are the Wild Cards, these can be placed whenever you want.";
        Game game = new Game();
        System.out.println("Welcome to our MAGNIFICENT UNO game!");
        boolean checkerOptions = false;
        int i = 0;
        while (!checkerOptions) {
            System.out.println(options);
            String optionInput = scannerInput();
            optionInput = optionInput.trim().toLowerCase();
            switch (optionInput) {
                case "start":
                    if (game.getPlayers().size() <2){
                        System.out.println("You need to have more than 2 Players\nPlease add Player before continuing\n");
                    }else {
                        System.out.println("Hands are given out");
                        System.out.println("If somebody has 1 card and doesnt say Uno type in the Name + didnt say Uno");
                        System.out.println("The first Player is: " + game.getPlayers().get(0).getUsername());
                        game.start();
                    }
                    break;
                case "setup":
                    setUpPlayers(game);
                    break;
                case "add/remove":
                case "add":
                case "remove":
                    removeAddPlayers(game);
                    break;
                case "info":
                    System.out.println(game.getPlayers());
                    break;

                    case "settings":
                    System.out.println("How many Cards do you want to be handed out?");
                    System.out.println("The current amount is: " + game.getHandOutCards());
                    int amountOfCards = scannerInput().charAt(0);
                    game.setAmountOfCards(amountOfCards);
                    break;
                case "rules":
                    System.out.println(rules);
                    break;
                case "exit":
                    checkerOptions = true;
                    break;

                    default:
                        System.out.println("Please enter a valid option");
                        break;
            }
        }
        System.out.println("Thanks for playing");
    }
}
