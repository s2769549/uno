package UNO.Client.Model.Factory;

import UNO.Client.Model.Player.ComputerPlayer;
import UNO.Server.Model.Deck.DeckConstants;
import UNO.Server.Model.Table.Table;
import UNO.Server.Model.Card.Card;

import java.util.*;

public abstract class Player{
    private final String username;
    private int points = 0;
    private boolean Uno;

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public ArrayList<Card> hand;

    /**
     * Constructor for Player
     * @param username the username of the player
     */
    public Player(String username) {
        this.username = username;
        this.hand = new ArrayList<>();
    }

    /**
     *
     * @return the player's username
     */
    public String getUsername() {
        return username;
    }

    public ArrayList<Card> getHand(){
        return hand;
    }

    /**
     * adds a Card to our players Hand
     * @param card - holds the card
     */

    public void addCardToHand(Card card){
        hand.add(card);
        if (!(this instanceof ComputerPlayer)) {
            System.out.println("You pulled from the Deck the Card : " + card);
        }
    }

    /**
     * determines the move of the player
     * @param table - the lobby in which the game is played
     */
    public abstract Card determineMove(Table table);

    /**
     * removes the desired card from the Hand of the Player
     * @param card - the selected card to be removed
     */
    public void handRemoveCard(Card card) {
        for (Card handCardRemove : hand) {
            if (handCardRemove.getCardValue().equals(card.getCardValue()) && handCardRemove.getColour().equals(card.getColour())) {
                hand.remove(handCardRemove);
                break;
            }
        }
    }

    /**
     * counts the amount of card the Plyer has to take for the plus2 and plus4 cards
     * also checks if it is a +2 on +4 or the other way around and adds the amount
      * @param table - the lobby in which the game is played
     */
    public void handAddCardsForPlusCard(Table table){
        int plus2 = 2;
        int plus4 = 4;
        int countPlusses = 0;
        boolean plussCount = true;
        while (plussCount){
            for (Card cardPlus : table.getUsedCards().getDeck()){
                if (cardPlus.getCardValue() == DeckConstants.CardsValue.WILDPLUS4){
                    countPlusses = countPlusses + plus4;
                }
                if (cardPlus.getCardValue() == DeckConstants.CardsValue.PLUS2){
                    countPlusses = countPlusses + plus2;
                }
                if (cardPlus.getCardValue() != DeckConstants.CardsValue.WILDPLUS4 && cardPlus.getCardValue() != DeckConstants.CardsValue.PLUS2){
                    plussCount = false;
                    break;
                }
            }
        }
        if (!(this instanceof ComputerPlayer)) {
            System.out.println("You have to take " + countPlusses + " cards!");
        }
        for (int i = 0; i<countPlusses;i++){
            if (table.getDeck().getDeck().size() < countPlusses){
                table.shuffleDeck();
            }
            Card getCard = table.getDeck().getDeck().get(0);
            addCardToHand(getCard);
            table.getDeck().removeCard(getCard);
        }
    }

    /**
     * checks if the player has a card that can be played
     * @param table - the lobby in which the game is played
     * @param card - the card that is played
     * @return true if the player has a card that can be played
     */
    public boolean determinAcceptableChoice(Table table , Card card){
        boolean result = determinInvalidNormalCard(table, card) && determineSpecial(table, card);
        if (card.getColour() == null || card.getCardValue() == null){
            result = false;
        }
        return result;
    }

    /**
     * grabs a Card from the table and checks if he still needs to grab cards
     * @param table - the lobby in which the game is played
     */
    public void grabCard(Table table){
        if (table.getUsedCards().getDeck().get(0).getCardValue() == DeckConstants.CardsValue.PLUS2 || table.getUsedCards().getDeck().get(0).getCardValue() == DeckConstants.CardsValue.WILDPLUS4){
            table.setGrabCard(true);
            handAddCardsForPlusCard(table);
        } else {
            Card getCard = table.getDeck().getDeck().get(0);
            addCardToHand(getCard);
            table.getDeck().removeCard(getCard);
        }
    }

    /**
     * checks if the card can be played
     * @param table - the lobby in which the game is played
     * @param card - the card that is played
     * @return true if the card can be played
     */
    public boolean determineSpecial(Table table, Card card){
        return determineIfPlusCardTop(table, card) && determineIfWildCardTop(table, card);
    }

    /**
     * determines if the Player can play the card if the top card of the deck is a wild card
     * @param table - the lobby in which the game is played
     * @param card - the card that is played
     * @return true if the card can be played
     */
    public boolean determineIfWildCardTop(Table table, Card card){
        String wishColourWrong = "This is not the colour that was wished";
        boolean result = true;
        Card topDeckCard = table.getUsedCards().getDeck().get(0);
        if (topDeckCard.getColour() == DeckConstants.Colors.BLACK){
            if (table.getUsedCards().getDeck().size() != 1) {
                if (card.getColour() != DeckConstants.Colors.BLACK) {
                    DeckConstants.Colors wishedColour = table.getColourWish();
                    if (card.getColour() != wishedColour) {
                        result = false;
                        if (!(this instanceof ComputerPlayer)) {
                            System.out.println(wishColourWrong);
                            System.out.println("The Colour that was wished is: " + wishedColour);
                        }
                    }
                }
            }
        }
        return result;
    }

    /**
     * checks if the top Card is a Plus2 or WildPlus4
     * if it is it will look if the played card is of the type +2 or WildPlus4
     * @param table - the lobby in which the game is played
     * @param card - the card that is played
     * @return true if the card can be played
     */
    public boolean determineIfPlusCardTop(Table table, Card card){
        Card topDeckCard = table.getUsedCards().getDeck().get(0);
        String notPlusCard = "The top card is a: " + topDeckCard +" therefore you can only put down a card that is of the value Plus2 or WildPlus4";
        String orGrabCard = "Or grab the amount of cards from the Deck using: grab Card";
        boolean result = true;
        if (!table.getGrabCard()) {
            if (table.getUsedCards().getDeck().size() != 1) {
                if (topDeckCard.getCardValue() == DeckConstants.CardsValue.PLUS2 || topDeckCard.getCardValue() == DeckConstants.CardsValue.WILDPLUS4) {
                    if (card.getCardValue() != DeckConstants.CardsValue.PLUS2 && card.getCardValue() != DeckConstants.CardsValue.WILDPLUS4) {
                        if (!(this instanceof ComputerPlayer)) {
                            System.out.println(notPlusCard);
                            System.out.println(orGrabCard);
                        }
                        result = false;
                    }
                }
            }
        }
        return result;
    }
    /**
     * a Card is valid if the player has thi Card in his Hand and
     * if the top card is the same Color or the same Value or the card is the color black
     * @param table - the lobby in which the game is played
     * @param card - the card that is played
     * @return true if the card is valid
     */
    public boolean determinInvalidNormalCard(Table table, Card card){
        boolean result = false;
        if (table.getUsedCards().getDeck().get(0).getColour() != DeckConstants.Colors.BLACK) {
            if (handContainsCard(card)) {
                Card topDeckCard = table.getUsedCards().getDeck().get(0);
                if (topDeckCard.getColour() == card.getColour() || topDeckCard.getCardValue() == card.getCardValue() || card.getColour() == DeckConstants.Colors.BLACK||topDeckCard.getColour() == null) {
                    result = true;
                } else {
                    if (!(this instanceof ComputerPlayer)) {
                        System.out.println("You cant put this card down look at the Rules!");
                        System.out.println("The top card is: " + table.getUsedCards().getDeck().get(0));
                    }
                }
            } else {
                System.out.println("You dont have this Card in your Hand");
            }
        }else {
            result = true;
        }
        return result;
    }

    /**
     * looks if our card that we input is in out Hand.
     * hand.contains doesn't work!!!!!!!!!!
     * @param card - holds the card value
     * @return true if the card is in our hand
     */
    public boolean handContainsCard(Card card){
        boolean result = false;
        for (Card handCard : hand){
            if (handCard.getColour() == card.getColour() && handCard.getCardValue() == card.getCardValue()) {
                result = true;
                break;
            }
        }
        return result;
    }

    /**
     * Sorts the Hand of the Player
     * @param table - the lobby in which the game is played
     */
    public void sortHand(Table table){
        Card topCard = table.getUsedCards().getDeck().get(0);

        ArrayList<Card> blackCards = new ArrayList<>();
        ArrayList<Card> yellowCard = new ArrayList<>();
        ArrayList<Card> redCards = new ArrayList<>();
        ArrayList<Card> greenCards = new ArrayList<>();
        ArrayList<Card> blueCards = new ArrayList<>();

        sortCards();
        for (Card card : hand){
            if (card.getColour() == DeckConstants.Colors.BLACK){
                blackCards.add(card);
            }else if (card.getColour() == DeckConstants.Colors.YELLOW){
                yellowCard.add(card);
            }else if (card.getColour() == DeckConstants.Colors.RED){
                redCards.add(card);
            }else if (card.getColour() == DeckConstants.Colors.GREEN){
                greenCards.add(card);
            }else if (card.getColour() == DeckConstants.Colors.BLUE){
                blueCards.add(card);
            }
        }
        hand.clear();

        DeckConstants.Colors sortAfterColour = topCard.getColour();
        if (topCard.getColour() == DeckConstants.Colors.BLACK){
            sortAfterColour = table.getColourWish();
        }
       if (sortAfterColour == DeckConstants.Colors.YELLOW) {
            hand.addAll(yellowCard);
            hand.addAll(blackCards);
            hand.addAll(redCards);
            hand.addAll(greenCards);
            hand.addAll(blueCards);
        }else if (sortAfterColour == DeckConstants.Colors.RED) {
            hand.addAll(redCards);
            hand.addAll(blackCards);
            hand.addAll(yellowCard);
            hand.addAll(greenCards);
            hand.addAll(blueCards);
        }else if (sortAfterColour == DeckConstants.Colors.GREEN) {
            hand.addAll(greenCards);
            hand.addAll(blackCards);
            hand.addAll(yellowCard);
            hand.addAll(redCards);
            hand.addAll(blueCards);
        }else if (sortAfterColour == DeckConstants.Colors.BLUE) {
            hand.addAll(blueCards);
            hand.addAll(blackCards);
            hand.addAll(yellowCard);
            hand.addAll(redCards);
            hand.addAll(greenCards);
        }
    }

    /**
     * Sorts the Hand of the Player after the Value of the Card
     */
    private void sortCards() {
        HashMap<Integer, Card> cardsRanks = new HashMap<>();
        for (Card card: hand) {

            boolean cardChecker = false;
            int valueAsNumber = card.cardValueAsNumber();

            while (!cardChecker){
                if (!cardsRanks.containsKey(valueAsNumber)) {
                    cardsRanks.put(valueAsNumber, card);
                    cardChecker = true;
                }
                valueAsNumber++;
            }
        }
        hand.clear();
        hand.addAll(cardsRanks.values());
        Collections.reverse(hand);

    }

    /**
     * determines the colour of the card
     * @param colourString - the colour that is input
     * @return the colour of the card
     */
    public DeckConstants.Colors determineColour(String colourString){
        DeckConstants.Colors colour = null;
        switch (colourString.toLowerCase()) {
            case "red":
                colour = DeckConstants.Colors.RED;
                break;
            case "blue":
                colour = DeckConstants.Colors.BLUE;
                break;
            case "black":
                colour = DeckConstants.Colors.BLACK;
                break;
            case "yellow":
                colour = DeckConstants.Colors.YELLOW;
                break;
            case "green":
                colour = DeckConstants.Colors.GREEN;
        }
        return colour ;
    }

    /**
     * determines the value of the card
     * @param valueString - the value that is input
     * @return the value of the card
     */
    public DeckConstants.CardsValue determineCardValue(String valueString){
        DeckConstants.CardsValue value = null;
        switch (valueString.toLowerCase()) {
            case "zero":
                value = DeckConstants.CardsValue.ZERO;
                break;
            case "one":
                value = DeckConstants.CardsValue.ONE;
                break;
            case "two":
                value = DeckConstants.CardsValue.TWO;
                break;
            case "three":
                value = DeckConstants.CardsValue.THREE;
                break;
            case "four":
                value = DeckConstants.CardsValue.FOUR;
                break;
            case "five":
                value = DeckConstants.CardsValue.FIVE;
                break;
            case "six":
                value = DeckConstants.CardsValue.SIX;
                break;
            case "seven":
                value = DeckConstants.CardsValue.SEVEN;
                break;
            case "eight":
                value = DeckConstants.CardsValue.EIGHT;
                break;
            case "nine":
                value = DeckConstants.CardsValue.NINE;
                break;
            case "plus2":
                value = DeckConstants.CardsValue.PLUS2;
                break;
            case "skip":
                value = DeckConstants.CardsValue.SKIP;
                break;
            case "reverse":
                value = DeckConstants.CardsValue.REVERSE;
                break;
            case "wild":
                value = DeckConstants.CardsValue.WILD;
                break;
            case "wildplus4":
                value = DeckConstants.CardsValue.WILDPLUS4;
                break;
        }
        return value;
    }
    /**
     * determines the move of the player in our Table
     * @param table - the table that is being played on
     */
    public void makeMove(Table table){
        sortHand(table);
        Card choice = determineMove(table);
        if (choice != null) {
            table.layDownCard(choice);
            handRemoveCard(choice);
        }
    }
}

