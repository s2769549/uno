package UNO.Client.Model.Player;

import UNO.Client.Model.Factory.Player;
import UNO.Server.Model.Deck.DeckConstants;
import UNO.Server.Model.Table.Table;
import UNO.Server.Model.Card.Card;

public class ComputerPlayer extends Player {

    boolean grabCard;
    public Card playedCard;
    public DeckConstants.Colors colorChoice;

    /**
     * creates a ComputerPlayer
     * @param username the username of the player
     */
    public ComputerPlayer(String username) {
        super(username);
    }

    /**
     * Determines the move of our BOT Player and gives back the Card that it choses
     * @param table - the lobby in which the game is played
     * @return the card the player chose
     */
    @Override
    public Card determineMove(Table table) {
        grabCard = false;
        Card result = null;
        Card firstCard = table.getUsedCards().getDeck().get(0);
        Card firstCardHand = hand.get(0);
        if (determinAcceptableChoice(table, firstCardHand)){//look if we can laydown the first card
            Card plusSkipReverse = determinePlus2SkipReverse(table.getUsedCards().getDeck().get(0).getColour(),table);//looks if we have a skip/reverse/plus2 that can be played
            if (plusSkipReverse != null) {
                result = plusSkipReverse;
            } else {
                result = firstCardHand;
            }
        }else if (!determinAcceptableChoice(table, firstCardHand)){//look if a value is the same then the first card
            for (Card card : hand){
                if (firstCard.getCardValue().equals(card.getCardValue())){
                    if (determinAcceptableChoice(table, card)){
                        result = card;
                    }
                }
            }
        }

        if (result != null && result.getColour().equals(DeckConstants.Colors.BLACK)){
            this.colorChoice = colorWish();
            table.setColourWish(colorChoice);
            System.out.println("The colour wish of the ComputerPlayer is: " + table.getColourWish());
        }
        if (result == null){
            System.out.println("The ComputerPlayer grabs a card");
            grabCard(table);
        }else {
            System.out.println(this.getUsername() + " plays the card: " + result);
        }
        this.playedCard = result;
        return result;
    }

    /**
     * Determines the Colour wish of the Computer based on the most cards in his hand
     * @return the colour wish of the Computer
     */
    public DeckConstants.Colors colorWish(){
        int countRed = 0;
        int countBlue = 0;
        int countGreen = 0;
        int countYellow = 0;

        for (Card card : hand) {
            if (card.getColour().equals(DeckConstants.Colors.RED)) {
                countRed++;
            } else if (card.getColour().equals(DeckConstants.Colors.BLUE)) {
                countBlue++;
            } else if (card.getColour().equals(DeckConstants.Colors.GREEN)) {
                countGreen++;
            } else if (card.getColour().equals(DeckConstants.Colors.YELLOW)) {
                countYellow++;
            }
        }
        int mostCards = 0;
        DeckConstants.Colors colour = null;
        if (countRed > mostCards){
            mostCards = countRed;
            colour = DeckConstants.Colors.RED;
        }
        if (countBlue > mostCards){
            mostCards = countBlue;
            colour = DeckConstants.Colors.BLUE;
        }
        if (countGreen > mostCards){
            mostCards = countGreen;
            colour = DeckConstants.Colors.GREEN;
        }
        if (countYellow > mostCards){
            mostCards = countYellow;
            colour = DeckConstants.Colors.YELLOW;
        }
        return colour;
    }

    /**
     * determines if a Plus2 Skip or Reverse can be played
     * @return a Card that is Plus2 Skip or Reverse if it can be played
     */
    public Card determinePlus2SkipReverse(DeckConstants.Colors colour, Table table){
        Card result = null;
        for (Card card : hand){
            if (determinAcceptableChoice(table,card)) {
                if (card.getCardValue().equals(DeckConstants.CardsValue.PLUS2) || card.getCardValue().equals(DeckConstants.CardsValue.SKIP) || card.getCardValue().equals(DeckConstants.CardsValue.REVERSE)) {
                    result = card;
                }
            }
        }
        return result;
    }

    @Override
    public String toString() {
        return getUsername();
    }
}
