package UNO.Client.Model.Player;

import UNO.Client.View.TUI;
import UNO.Client.Model.Factory.Player;
import UNO.Server.Model.Deck.DeckConstants;
import UNO.Server.Model.Card.Card;
import UNO.Server.Model.Table.Table;

import java.util.concurrent.TimeUnit;

public class

HumanPlayer extends Player {
    String choice = null;

    boolean grabCard = false;
    String choiceClient;
    private DeckConstants.Colors colourWished;

    /**
     * @param username the username of the player
     */
    public HumanPlayer(String username) {
        super(username);
    }

    /**
     * Determines the move of our Player and gives back the Card he choses
     * if the card is valid. The choice of card will be given out
     * @param table the table of the game
     * @return the card the player chose
     */
    public Card determineMove(Table table){
        grabCard = false;
        String prompt = "> " + getUsername() + " (" + hand + ")" + ", what is your choice? ";
        System.out.println(prompt);
        String choice = choiceInput();
        Card card = null;
        boolean valid = true;
        if (!choice.equalsIgnoreCase("grab card")){
            card = determineChoice(choice);
            valid = determinAcceptableChoice(table, card);
        }
        /**
         * we look if the input of the player is valid
         */
        while (!valid) {
                if (card.getColour() == null || card.getCardValue() == null) {
                    System.out.println("The Card input is not a valid Card. Something went wrong.");
                } else {
                    String errorCard = "ERROR: Card " + card + " is not a valid choice.";
                    System.out.println(errorCard);
                }
                System.out.println(prompt);
                choice = choiceInput();
                if (!grabCard){
                    card = determineChoice(choice);
                    valid = determinAcceptableChoice(table, card);
                }else {
                    valid = true;
                }
            }
        /**
         * if the player chooses to grab a card,we skip this part otherwise we determine the colour Wish of the Player
         */
        if (!grabCard) {
            if (card != null && card.getColour().equals(DeckConstants.Colors.BLACK)) {
                determineColourWish(table);
            }
        }
        /**
         * if the player chooses to grab a card, the card will be grabbed
         */
        if (choice.equalsIgnoreCase("grab card")){
            grabCard(table);
            card = null;
        }
        return card;
    }

    /**
     * determines the colour wish of the player
     * @param table the table of the game
     */
    public void determineColourWish(Table table){
        boolean colourWishNotNull = false;
        while (!colourWishNotNull) {
            System.out.println("What is your colour wish?");
            while (colourWished == null) {
                try {
                    TimeUnit.MILLISECONDS.sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            table.setColourWish(colourWished);
            colourWishNotNull = true;

        }
    }

    /**
     * determines the input of the Player. If the choice is a valid choice we return the choice.
     * @return the choice of the player
     */
    public String choiceInput(){
        String cardToLayDown = "Insert a card like: " + "Red / Five or Black / Wild or Green / Zero\nor use Command: grab Card";
        System.out.println(cardToLayDown);

        boolean choiceBoolean = false;
        this.choiceClient = null;
        while (!choiceBoolean) {
            while (choiceClient == null) {
                try {
                    TimeUnit.MILLISECONDS.sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            choice = choiceClient;
            String[] choiceSplit = choice.split(" ");
            if (choice.equalsIgnoreCase("exit")) {
                TUI.main(null);
            }
            int choiceTestIn = choice.split("/").length;
            if (choiceTestIn == 2) {
                choiceBoolean = true;
            }
            if (choice.equalsIgnoreCase("grab card")) {
                grabCard = true;
                choiceBoolean = true;
            }

            if (!choiceBoolean) {
                for (int i = 0; i < choiceSplit.length; i++) {
                    if (i + 2 < choiceSplit.length) {
                        if (choiceSplit[i].trim().equalsIgnoreCase("didnt")) {
                            if (choiceSplit[i + 1].trim().equalsIgnoreCase("say")) {
                                if (choiceSplit[i + 2].trim().equalsIgnoreCase("uno")) {
                                    choiceBoolean = true;
                                }
                            }
                        }
                    }
                }
            }

            if (!choiceBoolean) {
                System.out.println("The input is not a valid Input.");
            }
        }

        return choice;
    }

    /**
     * Determines the Card that the Player wants to choose out of his existing Cards in his Hand
     * @param choice - represents, out of the player's cards, the desired card by the player
     * @return the newly generated card by the player's choice
     */
    public Card determineChoice(String choice) {
        String choiceTrim = choice.replace(" ", "");
        String[] choice1 = choiceTrim.split("/");
        DeckConstants.Colors colour = null;
        DeckConstants.CardsValue value = null;
        if (choice1.length == 2) {
            colour = determineColour(choice1[0]);
            value = determineCardValue(choice1[1]);
        }
        return new Card(colour, value);
    }

    @Override
    public String toString() {
        return getUsername();
    }
    public void setChoiceClient(String choiceClient) {
        this.choiceClient = choiceClient;
    }

    public void setColourChoiceClient(String color) {
        this.colourWished = determineColour(color);
    }
}

