package UNO.Server.Model.Table;

import UNO.Server.Model.Deck.Deck;
import UNO.Client.Model.Factory.Player;
import UNO.Server.Model.Deck.DeckConstants;
import UNO.Server.Model.Card.Card;

import java.util.ArrayList;

public class Table {
    private Deck deck;
    public Deck getDeck() {
        return deck;
    }
    private final Deck usedCards;
    public Deck getUsedCards() {
        return usedCards;
    }
    public ArrayList<Player> players;
    private DeckConstants.Colors colourWish;
    private boolean grabCard = false;

    public boolean getGrabCard() {
        return grabCard;
    }

    public void setGrabCard(boolean grabCard) {
        this.grabCard = grabCard;
    }

    public DeckConstants.Colors getColourWish() {
        return colourWish;
    }

    public void setColourWish(DeckConstants.Colors colourWish) {
        this.colourWish = colourWish;
    }

    private final int playerCount;

    public int getPlayerCount() {
        return playerCount;
    }
    private int numberOfCards = 7;

    public void setNumberOfCards(int numberOfCards) {
       this.numberOfCards = numberOfCards;
    }

    public int getNumberOfCards() {
        return numberOfCards;
    }
    public Player winner;

    /**
     * Constructor
     * has a list of player and sets it to the current players
     * we calculate the player count
     * we create a new deck and set UP our deck
     * we create an empty deck of usedCards
     * @param players list of players
     */
    public Table(ArrayList<Player> players){
        this.players = players;
        this.playerCount = players.size();
        this.deck = new Deck();
        this.usedCards = new Deck();
    }

    /**
     * setsUP the Game
     * calls handOut function to hand out every player there Cards
     */
    public void setUP(){
        handOut();
    }

    /**
     * @return boolean if there is a winner or not
     */
    public boolean isWinner(){
        winner = null;
        for (Player player : players){
            if (player.hand.size() == 0){
                System.out.println("Winner: " + player);
                winner = player;
                calculatePoints(winner);
                return true;
            }
        }

        return false;
    }

    /**
     * calculates the points of the winner
     * @param winner the player who won
     */
    public void calculatePoints(Player winner){
        int points = 0;
        for (Player player : players){
            for (Card card : player.hand){
                points += card.getPoints(card);
            }
        }
        winner.setPoints(winner.getPoints() + points);
    }

    /**
     * if Game over it returns true
     * @return isWinner
     */
    public boolean gameOver(){
        return isWinner();
    }

    /**
     * first removes all cards of the deck and the usedCards
     * then setsUP the deck (puts all cards in the deck)
     * gives every player the number of cards to start the round
     * therefore every player gets 7 cards these cards will be removed from the deck
     */
    public void handOut(){
        deck.removeAll(deck);
        usedCards.removeAll(usedCards);
        deck.deckSetUp();

        for(Player player : players) {
            for (int n = 0; n < numberOfCards; n++) {
                Card card = deck.getDeck().get(0);
                deck.removeCard(card);
                player.hand.add(card);

            }
        }
        boolean notBlack = false;
        while (!notBlack){
            Card card = deck.getDeck().get(0);
            deck.removeCard(card);
            usedCards.addCard(card);
            if (card.getColour() != DeckConstants.Colors.BLACK){
                notBlack = true;
            }
        }
    }

    /**
     * will lay down a card onto the pile of usedCards
     * @param card card that will be layed down
     */
    public void layDownCard(Card card){
        usedCards.addCard(card);
    }

    /**
     * checks if our deck is the size of 3
     * @return true if size is 3
     */
    public boolean checkerEmpty(){
        return deck.getDeck().size() == 0;
    }

    /**
     * sets our deck to our usedCards
     * then removes all usedCards
     * and shuffles our deck
     */
    public void shuffleDeck(){
        Card topCard = usedCards.getDeck().get(0);
        Card secondCard = usedCards.getDeck().get(1);
        Card thirdCard = usedCards.getDeck().get(2);
        usedCards.removeCard(topCard);
        usedCards.removeCard(secondCard);
        usedCards.removeCard(thirdCard);
        for (Card card : usedCards.getDeck()){
            deck.addCard(card);
        }
        usedCards.removeAll(usedCards);
        usedCards.addCard(topCard);
        usedCards.addCard(secondCard);
        usedCards.addCard(thirdCard);
        deck.shuffle();
    }
    @Override
    public String toString() {
        return deck.getDeck().toString();
    }
}
