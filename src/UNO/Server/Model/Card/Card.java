package UNO.Server.Model.Card;

import UNO.Server.Model.Deck.DeckConstants;

public class Card {

    private final DeckConstants.Colors colors;
    private final DeckConstants.CardsValue cardValue;

    /**
     * Constructor to declare a color and a cardValue to our Card
     * @param color - holds the color of the card
     * @param cardValue - holds the value of the card
     */
    public Card(DeckConstants.Colors color, DeckConstants.CardsValue cardValue) {
        this.colors = color;
        this.cardValue = cardValue;
    }

    /**
     * gets the colour of the Card
     * @return the color of the card
     */
    public DeckConstants.Colors getColour() {
        return colors;
    }

    /**
     * gets the Value of the Card
     * @return the card value
     */
    public DeckConstants.CardsValue getCardValue() {
        return cardValue;
    }

    /**
     * gives Back an Integer of the Value of the Card
     */
    public Integer cardValueAsNumber(){
        int result = 0;
        if (cardValue == DeckConstants.CardsValue.ONE){
            result = 6;
        }
        else if (cardValue == DeckConstants.CardsValue.TWO){
            result = 12;
        }
        else if (cardValue == DeckConstants.CardsValue.THREE){
            result = 18;
        }
        else if (cardValue == DeckConstants.CardsValue.FOUR){
            result = 24;
        }
        else if (cardValue == DeckConstants.CardsValue.FIVE){
            result = 30;
        }
        else if (cardValue == DeckConstants.CardsValue.SIX){
            result = 36;
        }
        else if (cardValue == DeckConstants.CardsValue.SEVEN){
            result = 42;
        }
        else if (cardValue == DeckConstants.CardsValue.EIGHT){
            result = 48;
        }
        else if (cardValue == DeckConstants.CardsValue.NINE){
            result = 54;
        }
        else if (cardValue == DeckConstants.CardsValue.SKIP){
            result = 60;
        }
        else if (cardValue == DeckConstants.CardsValue.REVERSE){
            result = 66;
        }
        else if (cardValue == DeckConstants.CardsValue.PLUS2){
            result = 72;
        }
        else if (cardValue == DeckConstants.CardsValue.WILD){
            result = 78;
        }
        else if (cardValue == DeckConstants.CardsValue.WILDPLUS4){
            result = 84;
        }
        return result;
    }
    @Override
    public String toString() {
        return "[" + getColour() + " / " + getCardValue() + "]";
    }

    /**
     * returns the Card in a number of points
     * @return the number of points the card is worth
     */
    public int getPoints(Card card) {
        int points = 0;

        if (card.getCardValue() == DeckConstants.CardsValue.ZERO) {
            points = 0;
        }
        else if (card.getCardValue() == DeckConstants.CardsValue.ONE) {
            points = 1;
        }
        else if (card.getCardValue() == DeckConstants.CardsValue.TWO) {
            points = 2;
        }
        else if (card.getCardValue() == DeckConstants.CardsValue.THREE) {
            points = 3;
        }
        else if (card.getCardValue() == DeckConstants.CardsValue.FOUR) {
            points = 4;
        }
        else if (card.getCardValue() == DeckConstants.CardsValue.FIVE) {
            points = 5;
        }
        else if (card.getCardValue() == DeckConstants.CardsValue.SIX) {
            points = 6;
        }
        else if (card.getCardValue() == DeckConstants.CardsValue.SEVEN) {
            points = 7;
        }
        else if (card.getCardValue() == DeckConstants.CardsValue.EIGHT) {
            points = 8;
        }
        else if (card.getCardValue() == DeckConstants.CardsValue.NINE) {
            points = 9;
        }
        else if (card.getCardValue() == DeckConstants.CardsValue.PLUS2) {
            points = 20;
        }
        else if (card.getCardValue() == DeckConstants.CardsValue.SKIP) {
            points = 20;
        }
        else if (card.getCardValue() == DeckConstants.CardsValue.REVERSE) {
            points = 20;
        }
        else if (card.getCardValue() == DeckConstants.CardsValue.WILD) {
            points = 50;
        }
        else if (card.getCardValue() == DeckConstants.CardsValue.WILDPLUS4) {
            points = 50;
        }
        return points;
    }
}
