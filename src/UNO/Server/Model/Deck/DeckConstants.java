package UNO.Server.Model.Deck;


public final class DeckConstants {

    /**
     * Holds all the possible card values ranging from zero to nine, including the special cards - PLUS2, SKIP, REVERSE, WILD, WILDPLUS4
     */
    public enum CardsValue{
        ZERO,
        ONE,
        TWO,
        THREE,
        FOUR,
        FIVE,
        SIX,
        SEVEN,
        EIGHT,
        NINE,
        PLUS2,
        SKIP,
        REVERSE,
        WILD,
        WILDPLUS4
    }

    /**
     * holds the colors of the UNO cards - red, blue, green, yellow and black.
     */
    public enum Colors {
        RED,
        BLUE,
        GREEN,
        YELLOW,
        BLACK
    }




}
