package UNO.Server.Model.Deck;

import UNO.Server.Model.Card.Card;

import java.util.ArrayList;
import java.util.Collections;


public class Deck {
    private ArrayList<Card> deck;


    /**
     * creates a new Deck with all possible Cards
     */
    public Deck(){
        this.deck = new ArrayList<>();
    }

    /**
     * sets Up a new deck with all cards that are in the Uno Game
     */
    public void deckSetUp(){
        ArrayList<Card> saveDeck = new ArrayList<>();
        /**
         * Create every possible card two times
         */
        for (int y = 0; y <= 1 ; y ++) {
            for (DeckConstants.Colors color : DeckConstants.Colors.values()) {
                for (DeckConstants.CardsValue value : DeckConstants.CardsValue.values()) {
                    saveDeck.add(new Card(color, value));
                }
            }
        }
        /**
         * Check if the newly created deck contains black colored cards, zero values, wildplus4s and wilds, in case we come across one
         * it is skipped and not added into the final deck array list.
         */
        for (Card card : saveDeck){
            if (card.getColour() != DeckConstants.Colors.BLACK && card.getCardValue() != DeckConstants.CardsValue.ZERO && card.getCardValue() != DeckConstants.CardsValue.WILDPLUS4 && card.getCardValue() != DeckConstants.CardsValue.WILD ){
                deck.add(card);
            }
        }
        /**
         * Because we have previously removed all the mentioned above card appearances, we need  to add them back with the correct
         * amount, the wildplus4 and wild cards 4 times, and each color zero card one time. Deck in the end is also shuffled.
         */
        for (int o = 1; o <= 4; o ++) {
            deck.add(new Card(DeckConstants.Colors.BLACK, DeckConstants.CardsValue.WILD));
            deck.add(new Card(DeckConstants.Colors.BLACK, DeckConstants.CardsValue.WILDPLUS4));
        }
        deck.add(new Card(DeckConstants.Colors.BLUE, DeckConstants.CardsValue.ZERO));
        deck.add(new Card(DeckConstants.Colors.RED, DeckConstants.CardsValue.ZERO));
        deck.add(new Card(DeckConstants.Colors.GREEN, DeckConstants.CardsValue.ZERO));
        deck.add(new Card(DeckConstants.Colors.YELLOW, DeckConstants.CardsValue.ZERO));
        shuffle();
    }

    /**
     * Shuffles deck
     */
    public void shuffle(){
        Collections.shuffle(deck);
    }

    /**
     *
     * @return final deck, after it has been shuffled
     */
    public ArrayList<Card> getDeck() {
        return deck;
    }

    /**
     * adds the card to the deck at the index 0 (front)
     * @param card - card to be added
     */
    public void addCard (Card card){
        deck.add(0,card);
    }

    /**
     * removes the given card from out deck
     * @param card - card to be removed
     */
    public void removeCard(Card card){
        deck.remove(card);

    }

    public void setDeck(ArrayList<Card> deck) {
        this.deck = deck;
    }

    /**
     * removes all cards from the deck
     * @param deck - deck of cards to be removed
     */
    public void removeAll(Deck deck) {
        this.deck.removeAll(deck.getDeck());
    }
}
