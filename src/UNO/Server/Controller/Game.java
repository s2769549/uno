package UNO.Server.Controller;

import UNO.Client.Model.Factory.Player;
import UNO.Client.View.TUI;
import UNO.Server.Model.Deck.DeckConstants;
import UNO.Server.Model.Table.Table;
import UNO.Server.Model.Card.Card;

import java.util.*;
import java.util.concurrent.TimeUnit;

public class Game implements Runnable {
    private final Table table;

    public Table getTable() {
        return table;
    }

    private Card cardBefore;
    private Card cardBeforePlus;
    ArrayList<Player> players = new ArrayList<>();

    public ArrayList<Player> getPlayers() {
        return players;
    }

    private int currentPlayer = 0;

    public int getCurrentPlayer() {
        return currentPlayer;
    }

    public Player getPlayerCurrently(){
        return players.get(currentPlayer);
    }

    private Thread thread;

    private boolean running = false;

    public boolean startBoolean;
    public int rounds = 0;
    public String skippedPlayer;
    Player winnerOverall;
    public volatile boolean roundStartAgain = false;


    /**
     * Constructor for the Game
     */
    public Game(){
        this.table = new Table(players);
    }

    public void run(){
        start();
    }

    public synchronized void startThread(){
        if (running){
            return;
        }
        running = true;
        thread = new Thread(this);
        thread.start();
    }

    public synchronized void stopThread(){
        if (!running){
            return;
        }
        running = false;
        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public Player getLastPlayer(){
        int indexLast = currentPlayer-1;
        if (indexLast < 0){
            indexLast = players.size()-1;
        }
        return players.get(indexLast);

    }

    public Player getNextPlayer(){
        int indexNext = currentPlayer+1;
        if (indexNext > players.size()-1){
            indexNext = 0;
        }
        return players.get(indexNext);
    }

    /**
     * Method to start the Game. after a game is finished it will
     * ask if you want to play again
     */
    public void start() {
        boolean continueGame = true;
        while (continueGame) {

            reset();
            play();

            boolean winnerPoints = false;
            for (Player player : players){
                if (player.getPoints() >= 500){
                    System.out.println("Winner with more than 500 Points is: " + player);
                    winnerOverall = player;
                    winnerPoints = true;
                    startBoolean = false;
                }
            }
            if (winnerPoints){
                System.out.println("Do you want to reset the Points and play again? (y/n)");
                Scanner scanner = new Scanner(System.in);
                String answer = scanner.nextLine().trim().toLowerCase();
                continueGame = inputIntoBoolean(answer);
                for (Player player : players){
                    player.setPoints(0);
                }
            }else {
                roundStartAgain = false;
                while (!roundStartAgain) {
                    try {
                        TimeUnit.MILLISECONDS.sleep(1000);
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                }
            }
        }
        TUI.main(null);
    }

    /**
     * Leaderboard for the Game
     */
    public void leaderboard(){
        System.out.println("Leaderboard:");
        for (Player player : players){
            System.out.println(player.getUsername() + " has " + player.getPoints() + " Points");
        }

    }
    /**
     *this resets the Game because all Hands of the Players will be set to 0
     * the Handout method that will be called in the table.setUP will
     * remove all cards of our deck and used deck and create a new deck with all cards in it
     */

    public void reset(){
        this.rounds = 0;
        for (Player player:players){
            player.hand.removeAll(player.hand);
        }
        currentPlayer = 0;
    }

    /**
     * this method will be called in the start method
     * and plays the game until it is over
     */
    public void play(){
        table.setUP();
        while (!table.gameOver()) {
            if (table.checkerEmpty()) {
                table.shuffleDeck();
            }
            update();
            if (table.getUsedCards().getDeck().size() != 1) {
                if (cardBefore != table.getUsedCards().getDeck().get(1)) {
                    if (table.getUsedCards().getDeck().get(0).getCardValue() == DeckConstants.CardsValue.REVERSE) {
                        reverseCurrentPlayer();
                    }

                    if (table.getUsedCards().getDeck().get(0).getCardValue() == DeckConstants.CardsValue.SKIP) {
                        skipPlayer();
                    }
                }
                if (table.getUsedCards().getDeck().get(1) != cardBeforePlus) {
                    table.setGrabCard(false);
                }
                if (table.getUsedCards().getDeck().get(0).getCardValue() == DeckConstants.CardsValue.PLUS2 || table.getUsedCards().getDeck().get(0).getCardValue() == DeckConstants.CardsValue.WILDPLUS4) {
                    cardBeforePlus = table.getUsedCards().getDeck().get(1);
                }
            }
            players.get(currentPlayer).makeMove(table);
            updateCurrentPlayer();
            this.rounds++;
        }
        leaderboard();
    }

    /**
     * updates the current Player to the next player
     */
    public void updateCurrentPlayer(){
        if (currentPlayer < players.size()-1){
            currentPlayer++;
        }else {
            currentPlayer = 0;
        }
    }

    /**
     * reverses the current ArrayList of Players
     */
    public void reverseCurrentPlayer(){
        System.out.println("Reverse this GAME :D");
        Collections.reverse(table.players);
        cardBefore = table.getUsedCards().getDeck().get(1);
    }

    /**
     * skips the next Player
     */
    public void skipPlayer(){
        System.out.println(table.players.get(currentPlayer).getUsername() + " | YOU got Skipped ");
        try {
            TimeUnit.MILLISECONDS.sleep(1000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        this.skippedPlayer = table.players.get(currentPlayer).getUsername();
        this.currentPlayer = currentPlayer +1;
        if (this.currentPlayer > table.players.size()-1){
            this.currentPlayer = 0;
        }
        cardBefore = table.getUsedCards().getDeck().get(1);
    }

    /**
     * gives out the current status of the game
     * therefor the hand of the current player and the top card of our usedCards
     */
    public void update(){
        System.out.println("Current game situation Table: \n" + table.getUsedCards().getDeck().get(0));
        if (table.getUsedCards().getDeck().get(0).getColour() == DeckConstants.Colors.BLACK){
            System.out.println("The wished colour is: " + table.getColourWish());
        }
    }

    /**
     * adds a desired player to our Arraylist of players
     * @param player the player that will be added
     */
    public void addPlayer(Player player){
        if (players.size()<10) {
            players.add(player);
        }else {
            System.out.println("Lobby is full..." + "\n" + "Try again later!");
        }
    }

    /**
     * removes the desires Player in our ArrayList of players
     * @param player the player that will be removed
     */
    public void removePlayer(String player){
        for (Player player1 : players){
            if (player1.getUsername().trim().equalsIgnoreCase(player.trim())){
                for (Card card: player1.hand){
                    table.getDeck().getDeck().add(card);
                }
                players.remove(player1);
                System.out.println("Player "+ player1.getUsername() +" got removed");
                break;
            }
        }
    }

    /**
     * converts a String into a boolean
     * @param input the String input that will be checked
     * @return returns a boolean true/false
     */
    public static boolean inputIntoBoolean(String input){
        input = input.trim();
        boolean falseTrue = false;
        boolean result = false;
        while (!falseTrue){
            switch (input) {
                case "yes":
                case "ye":
                case "y":
                case "1":
                    result = true;
                    falseTrue = true;
                    break;
                    case "no":
                case "n":
                case "exit":
                case "0":
                    falseTrue = true;
                    break;
                default:
                    System.out.println("Invalid Input");
                    Scanner scanInput = new Scanner(System.in);
                    String scannedInput = scanInput.nextLine();
                    inputIntoBoolean(scannedInput);
                    break;
            }
        }

        return result;
    }

    @Override
    public String toString() {
        return getPlayers().toString();
    }

    /**
     * sets the desired amount of cards that the game starts with
     * @param amountOfCards the amount of cards that will be handed out
     */
    public void setAmountOfCards(int amountOfCards) {
        table.setNumberOfCards(amountOfCards);
    }

    /**
     * gets the amount of cards that the game starts with
     * @return returns the amount of cards that the game starts with
     */
    public int getHandOutCards() {
        return table.getNumberOfCards();
    }

    /**
     * removes all players from the game
     */
    public void removeAllPlayers() {
        players.removeAll(players);
    }
}
