package UNO.Server.Controller;

import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

public class Server{
    protected static Game game;

    protected static ArrayList<ServerHandler> serverHandlers;
    public static final int LISTENING_PORT = 3000;
    public static void main(String[] args) throws InterruptedException {

        System.out.println("SERVER | -> booting up ...");

        game = new Game();

        serverHandlers = new ArrayList<>();
        TimeUnit.MILLISECONDS.sleep(2000);

        System.out.println("SERVER | -> STATUS | -> up and running");

        TimeUnit.MILLISECONDS.sleep(2000);

        try(ServerSocket listener = new ServerSocket(LISTENING_PORT)) {

            System.out.println("SERVER | -> Listening on port | -> " + LISTENING_PORT);

            while (true){
                Socket clientSocket = listener.accept();
                ServerHandler serverHandler = new ServerHandler(clientSocket);
                serverHandlers.add(serverHandler);
                new Thread(serverHandler).start();
            }

        }catch (Exception e){

            System.err.println("SERVER | -> STATUS | -> down... | -> " + Arrays.toString(e.getStackTrace()));
            System.err.println("SERVER | -> Error | -> " + Arrays.toString(e.getStackTrace()));

        }
    }
}
