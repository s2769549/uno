package UNO.Server.Controller;

import UNO.Client.Model.Player.ComputerPlayer;
import UNO.Client.Model.Factory.Player;
import UNO.Client.Model.Player.HumanPlayer;
import UNO.Server.Model.Table.Table;
import UNO.Server.Model.Card.Card;

import java.io.*;
import java.net.Socket;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;


public class ServerHandler implements Runnable, ServerProtocol{
    private  Socket socket;
    private PrintWriter writer;
    private Player player;
    boolean myTurn;
    boolean cardPlayed;
    Card cardPlayedCard;
    boolean computerMessage;
    Player computer;
    boolean round = false;


    public ServerHandler(Socket clientSocket){
        this.socket = clientSocket;
        Server.game.startBoolean = false;
    }

    @Override
    public void run() {
        try {
            BufferedReader input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            writer = new PrintWriter(socket.getOutputStream(),true);

            while (true) {
                if (Server.game.startBoolean) {
                    TimeUnit.MILLISECONDS.sleep(1000);
                    if (Server.game.getLastPlayer() != computer){
                        computerMessage = false;
                    }
                    TimeUnit.MILLISECONDS.sleep(1000);
                    if (Server.game.rounds != 0) {
                        if (Server.game.getLastPlayer() instanceof ComputerPlayer) {
                            if (!computerMessage) {
                                ComputerPlayer x = (ComputerPlayer) Server.game.getLastPlayer();
                                if (x.playedCard == null){
                                    for (ServerHandler o : Server.serverHandlers) {
                                        o.doBroadcastDrewCard(x.getUsername());
                                    }
                                }else {
                                    for (ServerHandler o : Server.serverHandlers) {
                                        o.doBroadcastCardPlayed(Server.game.getLastPlayer().getUsername(), Server.game.getTable().getUsedCards().getDeck().get(0).toString());
                                    }
                                    TimeUnit.MILLISECONDS.sleep(1000);
                                    if (x.colorChoice != null){
                                        messageToClients("CW|" + x.colorChoice);
                                    }
                                }
                                computerMessage = true;
                                computer = Server.game.getLastPlayer();

                            }
                        }
                    }

                    if (cardPlayedCard != Server.game.getTable().getUsedCards().getDeck().get(0)) {
                        cardPlayed = false;
                    }

                    myTurn = false;
                    if (!cardPlayed) {
                        if (Objects.equals(Server.game.getPlayers().get(Server.game.getCurrentPlayer()), player)) {
                            this.doBroadcastGameInformation(Server.game.getTable().getUsedCards().getDeck().get(0).toString(), Server.game.getPlayers().get(Server.game.getCurrentPlayer()).getHand().toString(), Server.game.getPlayers().toString(), "true");
                            myTurn = true;
                        }
                    }

                    if (Server.game.getTable().isWinner()) {
                        if (!round) {
                            for (ServerHandler o : Server.serverHandlers) {
                                o.doRoundEnded(Server.game.getTable().winner.getUsername());
                                Server.game.roundStartAgain = false;
                                round = true;
                            }
                        }
                    }

                }
                long startTime = System.currentTimeMillis();
                int x = 2;
                String out = null;

                if (!myTurn) {
                    while ((System.currentTimeMillis() - startTime) < x * 1000 && !input.ready()) {
                    }
                    if (input.ready()) {
                        out = input.readLine();
                    }
                }else {
                    out = input.readLine();
                }

               if (out != null) {
                    String[] outSplit = out.split("\\|");
                    if (!Server.game.startBoolean || !Server.game.roundStartAgain) {
                        switch (outSplit[0]) {
                            case "MH":
                                handleHandshake(outSplit[1], outSplit[2]);
                                break;
                            case "ACP":
                                handleAddComputerPlayer(outSplit[1], outSplit[2]);
                                break;
                            case "SG":
                                if (round){
                                    Server.game.roundStartAgain = true;
                                    round = false;
                                }else {
                                    Server.game.startBoolean = true;
                                    handleStartGame(outSplit[1]);
                                }
                                break;
                            case "CL":
                                handleCreateLobby(outSplit[1]);
                                break;
                            case "JL":
                                handleJoinLobby(outSplit[1]);
                                break;
                            case "RP":
                                handleRemovePlayer(outSplit[1]);
                                break;
                            case "CC":
                            handleColorChoice(outSplit[1]);
                            break;

                        }
                    }
                    if (myTurn) {
                        switch (outSplit[0]) {
                            case "PC":
                                handlePlayCard(outSplit[1]);
                                break;
                            case "DC":
                                handleDrawCard();
                                break;
                            case "LG":
                                handleLeaveGame();
                                break;
                            case "SM":
                                handleSendMessage(outSplit[1]);
                                break;
                            case "CC":
                                handleColorChoice(outSplit[1]);
                                break;
                            case "RC":
                                handleRetainCard(outSplit[1]);
                                break;
                            case "MC7":
                                handleMakeChoiceSeven(outSplit[1], outSplit[2]);
                                break;
                        }
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    private void messageToAllClients(String message){
        for (ServerHandler s: Server.serverHandlers){
                s.writer.println(message);
        }
    }

    private void messageToClients(String message){
        for (ServerHandler s:Server.serverHandlers){
            if (!s.equals(this)){
                s.writer.println(message);
            }
        }
    }

    private ServerHandler switchNameToServerHandler(String name){
        ServerHandler currentPlayer = null;
        for (ServerHandler player: Server.serverHandlers){
            if (this.player.getUsername().equalsIgnoreCase(name)){
                currentPlayer = player;
            }
        }
        return currentPlayer;
    }
    public void handleRemovePlayer(String name){
        Server.game.removePlayer(name);
    }
    @Override
    public void handleHandshake(String playerName, String playerType) throws IOException {
        Player player = new HumanPlayer(playerName);
        this.player = player;
        for (Player checkPlayer : Server.game.getPlayers()) {
            if (player.getUsername().equals(checkPlayer.getUsername())) {
                doSendErrorCode(Errors.E002);
                socket.close();
                return;
            }
        }
        this.writer.println(ServerCommands.AH);
        Server.game.addPlayer(player);
        doBroadcastPlayerJoined(playerName);
    }

    @Override
    public void handleAddComputerPlayer(String playerName, String strategy) {
        Player cp = new ComputerPlayer(playerName+ "-Computer");
        if (!Server.game.getPlayers().contains(cp) && strategy.equals("computer_player")){
            Server.game.addPlayer(cp);
            messageToAllClients(ServerCommands.BPJ+"|" + playerName + "-Computer");
        }else{
            doSendErrorCode(Errors.E002);
        }
    }

    @Override
    public void handleStartGame(String gameMode) throws InterruptedException {
        Server.game.startThread();
        TimeUnit.MILLISECONDS.sleep(1000);
        for (ServerHandler handlers : Server.serverHandlers){
            handlers.doBroadcastGameMessage();
        }
    }

    @Override
    public void handlePlayCard(String card) {
        card = card.trim();
        String cardRefactor = card.replace(" ", "/");
        String[] cardSplit = cardRefactor.split("/");
        String result = null;
        boolean wild = false;
        if (cardSplit[1].equalsIgnoreCase("reverse")) {
            result = cardRefactor;
            messageToAllClients("BRS| LEFTRIGHT");
        }
        if (cardSplit[1].equalsIgnoreCase("skip")) {
            result = cardRefactor;
            messageToAllClients("BTS|" + Server.game.getNextPlayer().getUsername());
        }

        if (cardSplit[1].equalsIgnoreCase("plus2")) {
            result = cardRefactor;
        }
        if (cardSplit[1].equalsIgnoreCase("wild")) {
            result = "black/wild";
            messageToClients(cardSplit[0]);
            wild = true;
        }

        if (cardSplit[1].equalsIgnoreCase("draw_4")) {
            result = "black/wildplus4";
            messageToClients(cardSplit[0]);
            wild = true;
        }


        Pattern pattern = Pattern.compile("-?\\d+(\\.\\d+)?");
        if (pattern.matcher(cardSplit[1]).matches()) {
            String[] numbersToWords = {"zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten"};
            result = cardSplit[0] + "/" + numbersToWords[Integer.parseInt(cardSplit[1])];
        }
        //above is to refactor the card to the correct format
        if (result != null) {

            HumanPlayer p = (HumanPlayer) Server.game.getPlayerCurrently();
            Table table = Server.game.getTable();
            Card cardResult = p.determineChoice(result);

            if (p.determinAcceptableChoice(Server.game.getTable(), cardResult)) {

                if (wild) {
                    HumanPlayer x = (HumanPlayer) Server.game.getPlayerCurrently();
                    p.setColourChoiceClient(cardSplit[0]);
                    messageToClients("CW|" + cardSplit[0]);
                }

                p.setChoiceClient(result);
                cardPlayedCard = Server.game.getTable().getUsedCards().getDeck().get(0);
                cardPlayed = true;

                for (ServerHandler o : Server.serverHandlers) {
                    if (o != this) {
                        o.doBroadcastCardPlayed(p.getUsername(), result);
                    }
                }
            } else {
                this.writer.println("ERR|" + Errors.E010);
            }
        }
    }

    @Override
    public void handleDrawCard() {
        HumanPlayer p = (HumanPlayer) Server.game.getPlayerCurrently();
        p.setChoiceClient("grab card");
        doBroadcastDrewCard(p.getUsername());
        cardPlayedCard = Server.game.getTable().getUsedCards().getDeck().get(0);
        myTurn = false;
    }

    @Override
    public void handleLeaveGame() {
        Server.game.removePlayer(this.player.getUsername());
    }

    @Override
    public void handleCreateLobby(String lobbyName) {
    }

    @Override
    public void handleJoinLobby(String lobbyName) {
    }

    @Override
    public void handleSendMessage(String message) {
    }

    @Override
    public void handleSayUno() {
    }

    @Override
    public void handleRetainCard(String choice) {
    }

    @Override
    public void handleColorChoice(String color) {
    }

    @Override
    public void handleMakeChoiceSeven(String playerName, String card) {
    }

    @Override
    public void doAcceptHandshake() {
    }

    @Override
    public void doInformAdmin() {
        ServerHandler s = Server.serverHandlers.get(0);
        if (s.equals(this)){
            this.writer.println(ServerCommands.IAD);
        }
    }

    @Override
    public void doBroadcastPlayerJoined(String playerName) {
        for (ServerHandler handlers : Server.serverHandlers) {
            if (!handlers.equals(this)) {
                messageToClients(ServerCommands.BPJ + "|" + playerName);
            }
        }
        doInformAdmin();
    }

    @Override
    public void doBroadcastGameStarted(String gameMode) {
        messageToAllClients(ServerCommands.GST + "| test" + gameMode);
    }

    @Override
    public void doBroadcastRoundStarted() {
        messageToAllClients(String.valueOf(ServerCommands.RST));
    }

    @Override
    public void doBroadcastGameInformation(String topCard, String playerHand, String playersList, String isYourTurn) {
        for (ServerHandler handler : Server.serverHandlers){
          if (handler.player.getHand() != null){
              this.writer.println(ServerCommands.BGI + "|" + topCard + "|" + playerHand + "|" + Server.game.getPlayers().toString() + "|" + isYourTurn);
              break;
          }else if (handler.player.getPoints() == 500){
              doGameEnded(handler.player.getUsername());
              break;
          }else if (handler.player.getHand() == null){
              doRoundEnded(handler.player.getUsername());
              Server.game.reset();
              Server.game.getTable().setUP();
              this.writer.println(ServerCommands.BGI + "|" + topCard + "|" + playerHand + "|" + Server.game.getPlayers().toString() + "|" + isYourTurn);
              break;
          }
       }
    }

    @Override
    public void doBroadcastCardPlayed(String playerName, String playedCard) {
        this.writer.println(ServerCommands.BCP + "|"+playerName+ "|" +playedCard);
    }

    @Override
    public void doBroadcastDrewCard(String playerName) {
        for (ServerHandler handlers : Server.serverHandlers) {
            if (!handlers.equals(this)) {
                handlers.writer.println(ServerCommands.BDC + "|" + playerName);
            }
        }
        this.writer.println(ServerCommands.BDC + "|" + playerName);
    }

    @Override
    public void doBroadcastTurnSkipped(String playerName) {
        this.writer.println(ServerCommands.BTS  + "|"+playerName);
    }

    @Override
    public void doBroadcastReverse(String direction) {
        this.writer.println(ServerCommands.BRS  + "|" + direction);
    }

    @Override
    public void doBroadcastLeftGame(String playerName) {
        this.writer.println(ServerCommands.BLG + "|" + playerName);
    }

    @Override
    public void doRemindPlay(String timeLeft) {
        this.writer.println(ServerCommands.RP + "|"+timeLeft);
    }

    @Override
    public void doRoundEnded(String winnerName) {
        this.writer.println(ServerCommands.RE + "|" + winnerName);
    }

    @Override
    public void doGameEnded(String winnerName) {
        this.writer.println(ServerCommands.GE + "|" + winnerName);
    }

    @Override
    public void doSendErrorCode(Errors errorCode) {
        this.writer.println(ServerCommands.ERR + "|" + errorCode);
    }

    @Override
    public void doDrewPlayableCard(String card) {

    }

    @Override
    public void doAskColour() {
        this.writer.println(ServerCommands.AC);
    }

    @Override
    public void doBroadcastColourChange(String colour) {

    }

    @Override
    public void doBroadcastGameMessage(String... message) {
        this.writer.println(ServerCommands.BGM);
    }


    @Override
    public void handleClientDisconnected() {
        Server.serverHandlers.remove(this.switchNameToServerHandler(player.getUsername()));
    }

    @Override
    public void doBroadcastListOfLobbies(String lobbiesList) {
    }

    @Override
    public void doBroadcastCreatedLobby(String lobbyName) {
    }

    @Override
    public void doBroadcastPlayerJoinedLobby(String playerName) {
    }

    @Override
    public void doBroadcastMessage(String message) {
        this.writer.println(ServerCommands.BM);
    }

    @Override
    public void doBroadcastSayUNO(String playerName) {
    }
}
