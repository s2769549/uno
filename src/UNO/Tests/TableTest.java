package UNO.Tests;

import UNO.Client.Model.Factory.Player;
import UNO.Client.Model.Player.HumanPlayer;
import UNO.Server.Model.Deck.Deck;
import UNO.Server.Model.Deck.DeckConstants;
import UNO.Server.Model.Table.Table;
import UNO.Server.Model.Card.Card;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

public class TableTest {
    private Table table;
    private ArrayList<Player> players = new ArrayList<>();

    private Deck usedCards = new Deck();
    Card card = new Card(DeckConstants.Colors.RED, DeckConstants.CardsValue.ZERO);
    Card card1 = new Card(DeckConstants.Colors.RED, DeckConstants.CardsValue.WILDPLUS4);
    Card card2 = new Card(DeckConstants.Colors.RED, DeckConstants.CardsValue.WILD);
    Card card3 = new Card(DeckConstants.Colors.RED, DeckConstants.CardsValue.PLUS2);
    Card card4 = new Card(DeckConstants.Colors.RED, DeckConstants.CardsValue.EIGHT);
    Card card5 = new Card(DeckConstants.Colors.RED, DeckConstants.CardsValue.NINE);
    Card card6 = new Card(DeckConstants.Colors.RED, DeckConstants.CardsValue.PLUS2);
    Card card7 = new Card(DeckConstants.Colors.RED, DeckConstants.CardsValue.FOUR);
    Card card8 = new Card(DeckConstants.Colors.RED, DeckConstants.CardsValue.ONE);
    Card card9 = new Card(DeckConstants.Colors.RED, DeckConstants.CardsValue.TWO);
    Card card10 = new Card(DeckConstants.Colors.BLACK, DeckConstants.CardsValue.WILDPLUS4);
    Card card11 = new Card(DeckConstants.Colors.BLACK, DeckConstants.CardsValue.WILD);


    @BeforeEach
    public void setUp(){
        players.add(new HumanPlayer("seb"));
        players.add(new HumanPlayer("alex"));
        players.add(new HumanPlayer("leb"));
        this.table = new Table(players);

    }

    /**
     * Check if all the added players are playing at the table
     */
    @Test
    public void testNumberOfPlayers(){
        assertEquals(3,table.getPlayerCount());
    }

    /**
     * Check if the deck is empty
     */
    @Test
    public void testGetDeckEmpty(){
        assertEquals(0, table.getDeck().getDeck().size());
    }

    /**
     * Check when setting up the game, the player is handed out five cards.
     */
    @Test
    public void testCardsInHandOfPlayers(){
        table.setUP();
        assertEquals(7, players.get(0).getHand().size());

    }

    /**
     * Check if the used cards deck is of the size one after we set it up
     */
    @Test
    public void testUsedCardsSetUp(){
        table.setUP();
        table.getUsedCards().getDeck().add(1,card10);
        table.getUsedCards().getDeck().add(2,card11);
        boolean setUp = false;
        System.out.println(table.getUsedCards().getDeck().size());
        if (table.getUsedCards().getDeck().size() > 1){
            setUp = table.getUsedCards().getDeck().get(1).getColour() == DeckConstants.Colors.BLACK;
        }else if (table.getUsedCards().getDeck().size() == 1){
            setUp = true;
        }
        if (table.getUsedCards().getDeck().get(0).getColour() != DeckConstants.Colors.BLACK){
            setUp = true;
        }else {
            setUp = false;
        }
    }
    /**
     * Check if the deck containing the used cards is empty
     */
    @Test
    public void testEmptyUsedCards(){
        assertEquals(0, table.getUsedCards().getDeck().size());
    }

    /**
     * Check if the game has no winner by seeing if there is any player that has 0 cards left in their hand.
     */
    @Test
    public void testHasNoWinner(){
        table.setUP();
        assertFalse(table.gameOver());

    }

    /**
     * Check if the game has a winner by seeing if there's a player that has 0 cards in their hand.
     */
    @Test
    public void testHasWinner(){
        table.setUP();

        table.players.get(0).getHand().removeAll(table.players.get(0).getHand());
        assertTrue(table.gameOver());
    }

    /**
     * Check if the handout of the cards is correct
     */
    @Test
    public void testHandOutCards(){
        table.setUP();
        assertEquals(7, table.players.get(0).getHand().size());
        assertEquals(7, table.players.get(1).getHand().size());
        assertEquals(7, table.players.get(2).getHand().size());
    }
    /**
     * Check if laying a card down is correct
     */
    @Test
    public void testLayCardDown(){
        table.setUP();
        Card cardLayDown = table.players.get(0).getHand().get(0);
        table.layDownCard(table.players.get(0).getHand().get(0));
        assertEquals(cardLayDown, table.getUsedCards().getDeck().get(0));

    }
    /**
     * Tests if checkerEmpty works
     */
    @Test
    public void testCheckerEmpty(){
        table.setUP();
        table.getDeck().removeAll(table.getDeck());
        table.getDeck().getDeck().add(card);
        table.getDeck().getDeck().add(card1);
        table.getDeck().getDeck().add(card2);
        assertFalse(table.checkerEmpty());
        table.getDeck().removeAll(table.getDeck());
        assertTrue(table.checkerEmpty());
    }

    /**
     * Check remove all cards
     */
    @Test
    public void testRemoveAllCards(){
        table.getUsedCards().getDeck().add(card);
        table.getUsedCards().getDeck().add(card1);
        table.getDeck().getDeck().add(card2);
        table.getDeck().getDeck().add(card3);
        assertEquals(2, table.getDeck().getDeck().size());
        assertEquals(2, table.getUsedCards().getDeck().size());

        table.getDeck().removeAll(table.getDeck());
        assertEquals(0, table.getDeck().getDeck().size());
        assertEquals(2, table.getUsedCards().getDeck().size());

        table.getDeck().getDeck().add(card2);
        table.getDeck().getDeck().add(card3);
        assertEquals(2, table.getDeck().getDeck().size());
        assertEquals(2, table.getUsedCards().getDeck().size());

        table.getUsedCards().removeAll(table.getUsedCards());
        assertEquals(2, table.getDeck().getDeck().size());
        assertEquals(0, table.getUsedCards().getDeck().size());
    }

    /**
     * Test if shuffling works
     */
    @Test
    public void testShuffle(){


        table.getDeck().getDeck().add(card);
        table.getDeck().getDeck().add(card1);
        table.getUsedCards().getDeck().add(card2);
        table.getUsedCards().getDeck().add(card3);
        table.getUsedCards().getDeck().add(card4);
        table.getUsedCards().getDeck().add(card5);
        table.getUsedCards().getDeck().add(card6);
        table.getUsedCards().getDeck().add(card7);
        table.getUsedCards().getDeck().add(card8);
        table.getUsedCards().getDeck().add(card9);

        assertEquals(8, table.getUsedCards().getDeck().size());
        assertEquals(2, table.getDeck().getDeck().size());

        table.shuffleDeck();
        assertEquals(7, table.getDeck().getDeck().size());
        assertEquals(3, table.getUsedCards().getDeck().size());
    }
}
