package UNO.Tests;

import UNO.Server.Model.Deck.DeckConstants;
import UNO.Server.Model.Card.Card;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import UNO.Server.Model.Deck.Deck;


import static org.junit.jupiter.api.Assertions.*;

public class DeckTest {
    private Deck deck;
    private Deck usedCards;

    /**
     * This method, setUp creates each time a test runs a new shuffled cards, and used cards.
     */
    @BeforeEach
    public void setUp(){
        deck = new Deck();
        deck.deckSetUp();
        usedCards = new Deck();
    }

    /**
     *Check if the deck has 108 cards
     */
    @Test
    public void testDeckLength(){
        assertEquals(108, deck.getDeck().size());
    }
    /**
     * Check if there are 25 cards of color red
     */
    @Test
    public void testColorsRed() {
        int counterRed = 0;
        for (Card card : deck.getDeck()) {
            if (card.getColour() == DeckConstants.Colors.RED) {
                counterRed++;
            }
        }
        assertEquals(25, counterRed);
    }
    /**
     * Check if there are 25 cards of color blue
     */
    @Test
    public void testColorsBlue() {
        int counterBlue = 0;
        for (Card card : deck.getDeck()) {
            if (card.getColour() == DeckConstants.Colors.BLUE) {
                counterBlue++;
            }
        }
        assertEquals(25, counterBlue);
    }
    /**
     * Check if there are 25 cards of color green
     */
    @Test
    public void testColorsGreen() {
        int counterGreen = 0;
        for (Card card : deck.getDeck()) {
            if (card.getColour() == DeckConstants.Colors.GREEN) {
                counterGreen++;
            }
        }
        assertEquals(25, counterGreen);
    }
    /**
     * Check if there are 25 cards of color yellow
     */
    @Test
    public void testColorsYellow() {
        int counterYellow = 0;
        for (Card card : deck.getDeck()) {
            if (card.getColour() == DeckConstants.Colors.YELLOW) {
                counterYellow++;
            }
        }
        assertEquals(25, counterYellow);
    }

    /**
     * Check if there are 8 cards of color black
     */
    @Test
    public void testColorsBlack() {
        int counterBlack = 0;
        for (Card card : deck.getDeck()) {
            if (card.getColour() == DeckConstants.Colors.BLACK) {
                counterBlack++;
            }
        }
        assertEquals(8, counterBlack);
    }

    /**
     * Check if we are able to remove a card from the main deck
     */
    @Test
    public void testRemoveCard(){
        Card card1 = new Card(DeckConstants.Colors.BLACK, DeckConstants.CardsValue.WILDPLUS4);
        Card card2 = new Card(DeckConstants.Colors.BLACK, DeckConstants.CardsValue.WILD);
        Card card3 = new Card(DeckConstants.Colors.RED, DeckConstants.CardsValue.ZERO);
        deck.addCard(card1);
        deck.addCard(card2);
        deck.addCard(card3);
        assertEquals(card1, deck.getDeck().get(2));
        assertEquals(card2, deck.getDeck().get(1));
        assertEquals(card3, deck.getDeck().get(0));
        deck.removeCard(card1);
        deck.removeCard(card2);
        assertEquals(card3, deck.getDeck().get(0));
    }

    /**
     * Check if we are able to add a card to the empty usedCards deck.
     */
    @Test
    public void testAddCard(){
        Card card1 = new Card(DeckConstants.Colors.BLACK, DeckConstants.CardsValue.WILD);
        usedCards.addCard(card1);
        assertEquals(card1, usedCards.getDeck().get(0));
    }

    /**
     * Check removeAll cards from Deck
     */
    @Test
    public void testRemoveAllCards(){
        Card card1 = new Card(DeckConstants.Colors.BLACK, DeckConstants.CardsValue.WILD);
        Card card2 = new Card(DeckConstants.Colors.BLACK, DeckConstants.CardsValue.WILDPLUS4);
        Card card3 = new Card(DeckConstants.Colors.RED, DeckConstants.CardsValue.ZERO);
        deck.addCard(card1);
        deck.addCard(card2);
        deck.addCard(card3);
        deck.removeAll(deck);
        assertEquals(0, deck.getDeck().size());
        usedCards.addCard(card1);
        usedCards.addCard(card2);
        usedCards.addCard(card3);
        assertEquals(3, usedCards.getDeck().size());
        usedCards.removeAll(usedCards);
        assertEquals(0, usedCards.getDeck().size());
    }

    /**
     * Check our setDeck method
     */
    @Test
    public void testSetDeck(){
        deck.removeAll(deck);
        assertEquals(0, deck.getDeck().size());
        usedCards.removeAll(usedCards);
        assertEquals(0, usedCards.getDeck().size());
        Card card1 = new Card(DeckConstants.Colors.BLACK, DeckConstants.CardsValue.WILD);
        Card card2 = new Card(DeckConstants.Colors.BLACK, DeckConstants.CardsValue.WILDPLUS4);
        Card card3 = new Card(DeckConstants.Colors.RED, DeckConstants.CardsValue.ZERO);
        usedCards.addCard(card1);
        usedCards.addCard(card2);
        usedCards.addCard(card3);
        assertEquals(0, deck.getDeck().size());
        assertEquals(3, usedCards.getDeck().size());
        deck.setDeck(usedCards.getDeck());
        assertEquals(3, deck.getDeck().size());
    }
}
