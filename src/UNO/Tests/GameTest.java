package UNO.Tests;

import UNO.Client.Model.Player.HumanPlayer;
import UNO.Server.Controller.Game;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


public class GameTest {
    private Game uno;

    /**
     * Create a new game each time we test some aspects of our UNO game.
     */
    @BeforeEach
    public void setUp(){
        uno = new Game();
    }

    /**
     * Test if the game is created correctly.
     */
    @Test
    public void testGame(){
        assertNotNull(uno);
    }
    /**
     * test if we can add Players to our game
     */
    @Test
    public void testAddPlayer(){
        uno.addPlayer(new HumanPlayer("test"));
        assertEquals(1, uno.getPlayers().size());
    }

    /**
     * test if we can remove Players from our game
     */
    @Test
    public void testRemovePlayer(){
        uno.addPlayer(new HumanPlayer("test"));
        uno.addPlayer(new HumanPlayer("test2"));
        assertEquals(2, uno.getPlayers().size());
        uno.removePlayer("test");
        assertEquals(1, uno.getPlayers().size());
    }
    /**
     * Check if the lobby's current player count is bellow the limit (10).
     */
    @Test
    public void lobbyNotFull(){
        uno.addPlayer(new HumanPlayer("Player1"));
        uno.addPlayer(new HumanPlayer("Player2"));
        uno.addPlayer(new HumanPlayer("Player3"));
        assertEquals(3, uno.getPlayers().size());
    }

    /**
     * Check if the lobby's current player count exceeds the player limit (10).
     */
    @Test
    public void lobbyIsFull(){
        uno.addPlayer(new HumanPlayer("Alex"));
        uno.addPlayer(new HumanPlayer("Bob"));
        uno.addPlayer(new HumanPlayer("Cindy"));
        uno.addPlayer(new HumanPlayer("David"));
        uno.addPlayer(new HumanPlayer("Eve"));
        uno.addPlayer(new HumanPlayer("Frank"));
        uno.addPlayer(new HumanPlayer("George"));
        uno.addPlayer(new HumanPlayer("Helen"));
        uno.addPlayer(new HumanPlayer("Ivan"));
        uno.addPlayer(new HumanPlayer("John"));
        uno.addPlayer(new HumanPlayer("Karl"));

        assertEquals(10, uno.getPlayers().size());

    }

    /**
     * Checks if set Amount of Cards is set correctly
     */
    @Test
    public void setAmountOfCards(){
        uno.setAmountOfCards(4);
        assertEquals(4, uno.getHandOutCards());
    }


}
