package UNO.Tests;

import UNO.Client.Model.Factory.Player;
import UNO.Client.Model.Player.HumanPlayer;
import UNO.Server.Model.Deck.DeckConstants;
import UNO.Server.Model.Card.Card;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class PlayerTest {
    private Player player;

    /**
     * Before every test there is a new HumanPlayer created.
     */
    @BeforeEach
    public void setUp(){
         player = new HumanPlayer("Player 1");

    }

    /**
     * Check if the player has an empty hand.
     */
    @Test
    public void testEmptyHand(){
        assertEquals(0, player.getHand().size());
    }

    /**
     * Check if there can be cards added to player's hand
     */
    @Test
    public void testAddCardToHand(){
        Card card1 = new Card(DeckConstants.Colors.RED, DeckConstants.CardsValue.WILD);
        player.addCardToHand(card1);
        assertEquals(1, player.getHand().size());


        Card card2 = new Card(DeckConstants.Colors.GREEN, DeckConstants.CardsValue.EIGHT);
        Card card3 = new Card(DeckConstants.Colors.YELLOW, DeckConstants.CardsValue.SKIP);
        Card card4 = new Card(DeckConstants.Colors.RED, DeckConstants.CardsValue.WILDPLUS4);
        player.addCardToHand(card2);
        player.addCardToHand(card3);
        player.addCardToHand(card4);

        assertEquals(4, player.getHand().size());

    }

    /**
     * Check if there can be cards removed from the player's hand
     */
    @Test
    public void testRemoveCardFromHand(){
        Card card1 = new Card(DeckConstants.Colors.RED, DeckConstants.CardsValue.WILD);
        player.addCardToHand(card1);
        assertEquals(1, player.getHand().size());


        Card card2 = new Card(DeckConstants.Colors.GREEN, DeckConstants.CardsValue.EIGHT);
        Card card3 = new Card(DeckConstants.Colors.YELLOW, DeckConstants.CardsValue.SKIP);
        Card card4 = new Card(DeckConstants.Colors.RED, DeckConstants.CardsValue.WILDPLUS4);
        player.addCardToHand(card2);
        player.addCardToHand(card3);
        player.addCardToHand(card4);
        assertEquals(4, player.getHand().size());

        player.handRemoveCard(card1);
        assertEquals(3, player.getHand().size());

        player.handRemoveCard(card2);
        player.handRemoveCard(card3);
        player.handRemoveCard(card4);

        assertEquals(0, player.getHand().size());


    }
}
