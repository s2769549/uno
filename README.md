# ***UNO***


###
Original UNO rules -> [UNO](https://www.unorules.com/)



### Starting the Server and the Client:

To start playing UNO, the following steps need to be checked off in the following list:
1. SDK: 11 java version "11.0.17".
2. Language Level: SDK default (11-Local variable syntax for lambda parameters).
3. Libraries: junit.jupiter.
4. The `Server` class should be run inside an IDE - preferably IntelliJ (When the `Server` starts running, an update will be sent through the console displaying the STATUS of the Server and the port on which is listening).
5. The `Client` class should be run an IDE - preferably IntelliJ (The number of **HumanPlayers** in the game is determined by the amount of instances of `Client` that are actively running. Keep in mind the `Server` can hold up to 10 players - **including BOTS**).
6. Wait until `Client` has received the message; "successfully prepared" and you are asked to provide the desired port.

### Establishing the connection between Client and Server:

1. The `Client` has to provide the port number **3000** after the message: "Please provide the desired port"
2. After the Client displays the message: "Please input the server address", provide the following server address: **localhost**.
3. The Client has connected successfully when the messages ***"Connected to the server"*** and ***"Please enter a username"*** are displayed.
4. If the port or server address are wrongly entered, you have to re-run the `Client` to re-enter the correct port number and server address.

### Choosing a username:

1. Any username is allowed as long as it's not already in use.
2. In case the username is already in use, the `Client` class has to be re-run and a valid name has to be entered.
3. The Admin is the first HumanPlayer to join the game and is the only one that can start the game. To start the game at least two players have to be connected to the Server (Human or Computer). 
4. **ComputerPlayers can be added only by the Admin**. They can be added with the following command - "add / [any name] computer_player".
5. ***Unfortunately an all computer game isn't possible yet*** (Because adding of the bot is dependent on the Admin - the first HumanPlayer).
6. After a username has been successfully registered a confirmation message will be printed to all the clients in the game that a new player has joined.

### Starting the game:

1. To start a game ***the Admin*** has to enter the following command: "play / [desired game mode]". **Example: play / progressive**. ***Note: our game supports only Progressive UNO (+2)***
2. After the game started, specific GameInfo will be displayed for each player. In addition, GameInfo provides HELP tips on how to make a move.
3. For rules, please check the following link -> [Click me](https://www.unorules.com/)

### Playing a card:

1. To play a regular card, the player has to enter the following command: play / [card color] [number of the card], Example - play / red 6.
2. To play a special card excluding the black wild cards, (Reverse, Skip, plus2), the player has to enter the following command: play / [card color - word] [the special part of the card], Example - play / green plus2, play / red skip.
3. To play a normal black wild card, the player has to enter the following command: play / [Color wish to change to] [wild], Example - play / green wild, where green is the color to be changed to.
4. To play a "black wild plus 4", the player should enter the following command: play / [color wish to change to] [draw_4], Example - play / green draw_4, where green is the color to be changed to.
5. In case of an invalid move, wait until the GameInfo is displayed again.
6. For rules, please check the following link -> [Click me](https://www.unorules.com/)

### When a round/game is finished:

1. A game is finished only when a participant reaches 500 points. A game has as many rounds as it's needed for a participant to reach 500 points.
2. A round is finished only when a participant's hand is empty.
3. After a game has finished there's the option to play again by typing ***start*** / progressive or ***stop*** by closing the `Client` or the `Server`.


### Authors:

***Sebastian Schierholz -> s27489830 (s.d.h.schierholz@student.utwente.nl)*** <br />
***Condu Alexandru - Stefan -> s2769549 (a.condu@student.utwente.nl)***
